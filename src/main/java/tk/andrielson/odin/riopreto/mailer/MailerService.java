package tk.andrielson.odin.riopreto.mailer;

public interface MailerService {
    void sendEmail(EmailMessage message);
}
