package tk.andrielson.odin.riopreto.mailer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailerServiceImpl implements MailerService {

    private final JavaMailSender mailSender;

    @Override
    public void sendEmail(final EmailMessage message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            messageHelper.setFrom("andrielson@carroporassinatura.blog.br", "Boletim Diário Oficial - Rio Preto");
            messageHelper.setTo(message.getTo());
            messageHelper.setSubject(message.getSubject());
            var t = message.getText();
            var h = message.getHtml();
            messageHelper.setText(t, h);
        };
//        try {
//            mailSender.send(messagePreparator);
//            log.info("Activation email sent!");
//        } catch (MailException e) {
//            log.debug(e.getMessage());
//            log.error("Exception occurred when sending mail to " + message.getTo());
//        }
        mailSender.send(messagePreparator);
    }
}
