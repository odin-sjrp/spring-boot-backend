package tk.andrielson.odin.riopreto.subscribers;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Document("subscribers")
public class Subscriber {
    @Id
    private String id;

    @Indexed(unique = true)
    private String email;
    private List<String> keywords;
    private LocalDateTime createdAt;
    private LocalDateTime verifiedAt;
}
