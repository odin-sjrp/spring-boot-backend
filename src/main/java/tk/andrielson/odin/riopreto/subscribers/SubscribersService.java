package tk.andrielson.odin.riopreto.subscribers;

import org.springframework.lang.NonNull;

public interface SubscribersService {
    void create(@NonNull CreateSubscriberDto createSubscriberDto);
}
