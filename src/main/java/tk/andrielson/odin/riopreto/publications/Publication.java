package tk.andrielson.odin.riopreto.publications;

public record Publication(String code, String link) {
}
