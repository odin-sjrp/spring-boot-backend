package tk.andrielson.odin.riopreto.mailer;

import org.springframework.lang.NonNull;
import org.thymeleaf.context.Context;

public class SignupConfirmation implements EmailMessage {

    private final String subject = "Confirme sua inscrição";
    private final String to;
    private final Context context = new Context();

    public SignupConfirmation(@NonNull String to, @NonNull String confirmationLink, @NonNull String[] keywords) {
        this.to = to;
        context.setVariable("confirmationLink", confirmationLink);
        context.setVariable("keywords", keywords);
    }

    @Override
    public @NonNull
    String getHtml() {
        return EmailBuilder.HTML.getTemplateEngine().process("signup-confirmation.html", context);
    }

    @Override
    public @NonNull
    String getText() {
        return EmailBuilder.TEXT.getTemplateEngine().process("signup-confirmation.txt", context);
    }

    @Override
    public @NonNull
    String getSubject() {
        return subject;
    }

    @Override
    public @NonNull
    String getTo() {
        return to;
    }
}
