package tk.andrielson.odin.riopreto.publications;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import tk.andrielson.odin.riopreto.subscribers.SubscribersRepository;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

@Slf4j
@Service
@RequiredArgsConstructor
public class PublicationsServiceImpl implements PublicationsService {

    private final SubscribersRepository subscribersRepository;

    @Override
    public Map<String, Map<String, List<Publication>>> teste() {
        return teste001();
    }

    private @NonNull
    Map<String, Map<String, List<Publication>>> teste001() {
        final var keywordSet = new ConcurrentSkipListSet<String>();
        final var searchResults = new ConcurrentHashMap<String, List<Publication>>();
        final var subscribers = subscribersRepository.findAll();
        final var subscribersResults = new ConcurrentHashMap<String, Map<String, List<Publication>>>();

        subscribers.stream().parallel()
                .forEach(subscriber -> keywordSet.addAll(subscriber.getKeywords().size() > 0 ? subscriber.getKeywords() : Collections.singletonList("")));

        keywordSet.stream().parallel()
                .forEach(keyword -> {
                    var result = searchKeyword(keyword);
                    if (result.size() > 0)
                        searchResults.put(keyword, result);
                });

        subscribers.stream().parallel()
                .forEach(subscriber -> {
                    if (subscriber.getKeywords().size() == 0) {
                        subscribersResults.put(subscriber.getEmail(), Collections.singletonMap("", searchResults.get("")));
                        return;
                    }
                    var resultsForSubscriber = new HashMap<String, List<Publication>>();
                    for (var keyword : subscriber.getKeywords()) {
                        if (!searchResults.containsKey(keyword))
                            break;
                        resultsForSubscriber.put(keyword, searchResults.get(keyword));
                    }
                    subscribersResults.put(subscriber.getEmail(), resultsForSubscriber);
                });
        return subscribersResults;
    }

    private @NonNull
    List<Publication> searchKeyword(@NonNull String keyword) {
        final var linkPrefix = "https://www.riopreto.sp.gov.br/DiarioOficial/";
        final var map = new HashMap<String, String>(3);
        map.put("Diario!listar.action", "Buscar");
        map.put("diario.dataPublicacao", "18/08/2021");
        map.put("diario.palavraChave", keyword);

        try {
            return Jsoup.connect(linkPrefix + "Diario!listar.action")
                    .data(map)
                    .post()
                    .body()
                    .getElementsByAttributeValueStarting("href", "Diario!arquivo.action?diario.codPublicacao=")
                    .stream()
                    .map(element -> new Publication(element.attr("href").split("=")[1], linkPrefix + element.attr("href")))
                    .toList();
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }
}
