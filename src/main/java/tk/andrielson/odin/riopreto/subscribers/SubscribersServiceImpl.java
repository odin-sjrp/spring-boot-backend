package tk.andrielson.odin.riopreto.subscribers;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SubscribersServiceImpl implements SubscribersService {

    private final SubscribersRepository subscribersRepository;

    @Override
    public void create(@NonNull CreateSubscriberDto createSubscriberDto) {
        var optionalSubscriber = subscribersRepository.findByEmail(createSubscriberDto.getEmail());
        var subscriber = optionalSubscriber.orElse(new Subscriber());
        subscriber.setEmail(createSubscriberDto.getEmail());
        subscriber.setKeywords(createSubscriberDto.getKeywords());
        subscriber.setCreatedAt(createSubscriberDto.getCreatedAt());
        subscribersRepository.save(subscriber);
    }
}
