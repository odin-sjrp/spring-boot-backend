package tk.andrielson.odin.riopreto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OdinRioPretoApplication {

	public static void main(String[] args) {
		SpringApplication.run(OdinRioPretoApplication.class, args);
	}

}
