package tk.andrielson.odin.riopreto.subscribers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateSubscriberDto {
    @Email
    @NotNull(message = "O e-mail não pode ser nulo")
    private String email;

    @NotNull(message = "O consentimento é obrigatório")
    @AssertTrue(message = "O consentimento é obrigatório")
    private Boolean consent;

    @NotNull(message = "A lista de palavras não pode ser nula")
    private List<String> keywords;

    private final LocalDateTime createdAt = LocalDateTime.now();
}
