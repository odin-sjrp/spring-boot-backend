package tk.andrielson.odin.riopreto.subscribers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.andrielson.odin.riopreto.publications.Publication;
import tk.andrielson.odin.riopreto.publications.PublicationsService;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/subscribers")
@RequiredArgsConstructor
public class SubscribersController {
    private final SubscribersService subscribersService;
    private final PublicationsService publicationsService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Map<String, List<Publication>>>> create(@Valid @RequestBody CreateSubscriberDto createSubscriberDto) {
        /*this.subscriberService.create(createSubscriberDto);
        return ResponseEntity.accepted().build();*/
        return ResponseEntity.ok(publicationsService.teste());
    }

}
