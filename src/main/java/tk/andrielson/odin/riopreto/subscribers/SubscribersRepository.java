package tk.andrielson.odin.riopreto.subscribers;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

public interface SubscribersRepository extends MongoRepository<Subscriber, String> {
    Optional<Subscriber> findByEmail(@NonNull String email);
}
